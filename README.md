# Portia firmware analysis

This repository is about the binary analysis of the spff firmware as part of
the SetAPP v1.0.2 android application. The firmware file has version 4.0.58.

## Update image
The spff image turns out to be a [swupdate (.swu)](http://sbabic.github.io/swupdate/) image
and after extraction using [Armijn Hemel](https://github.com/armijnhemel)'s [BANG](https://github.com/armijnhemel/binaryanalysis-ng) tool
the image was decomposed into the following files:

[sw-description](sw-description) ([doc](https://github.com/sbabic/swupdate/blob/master/doc/source/sw-description.rst)) describing the firmware update

[setup.sh](setup.sh) post firmware update setup script

zImage Linux kernel

sama5d2_d1186b.dtb device tree blob

sama5d2_d1186ba.dtb device tree blob

rootfs.ubfifs

## Device tree information
The firmware image consists of 2 device tree blobs, which can be decompiled.
[sama4d2_d1186b.dtb](sama5d2_d1186b.dts)
[sama5d2_d1186ba.dtb](sama5d2_d1186ba.dts)

Glancing over them, we can see that they are very similar and build upon the
[Atmel SAMA5D2 Xplained](https://github.com/linux4sam/linux-at91/blob/linux4sam_5.7/arch/arm/boot/dts/at91-sama5d2_xplained.dts) ([mainline](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/tree/arch/arm/boot/dts/at91-sama5d2_xplained.dts?h=v4.9.52)) tree/board.

When comparing the sama5d2_d1186b.dts to the compiled, and [decompiled upstream device tree](at91-sama5d2_xplained.dts), a few interesting things come to notion.

```
Extra serial ports enabled:
	serial2 = "/ahb/apb/flexcom@f8034000/serial@200";
	serial3 = "/ahb/apb/flexcom@fc018000/serial@200";
	serial4 = "/ahb/apb/flexcom@fc010000/serial@200";
	serial5 = "/ahb/apb/serial@fc00c000";

sdmmc1 is disabled
qspi0 is disabled
i2c0 and the PMIC 'active-semi,act8945a' are disabled
pwm0 is enabled
flx0 is enabled
  uart5 as rs485 is enabled
	linux,rs485-enabled-at-boot-time;
	rs485-rts-delay = <0x0 0xc8>;
uart3 is enabled
uart4 is enabled
flx2 is enabled
  serial4 is enabled
flx4 is enabled
  serial3 as rs485 is enabled replacing i2c2
i2c1 is disabled
isc_base is removed from the pinmux controller
isc_data_8bit is removed from the pinmux controller
sensor_power is removed from the pinmux controller
sensor_reset is removed from the pinmux controller
i2c0_default is removed from the pinmux controller
i2c1_default is removed from the pinmux controller
flx4_default is added to the pinmux controller
pinctrl_lcd_pwm is removed from the pinmux controller
out_gpio_default is added to the pinmux controller
sdmmc0_default pull-up is enabled on cmd_data and ck_rstn_vddsel
sdmmc1_default pull-up is enabled on cmd_data
uart4_default is added to the pinmux controller
pwm0_pwm2_default is removed from the pinmux controller
classd is disabled
gpio_keys:bp1 has its wakeup-source removed
gpio_keys:pri_0 was added with linux keycode 0x105
gpio_keys:pri_1 was added with linux keycode 0x106
gpio_keys:pri_2 was added with linux keycode 0x107
gpio_keys:pri_3 was added with linux keycode 0x108
leds:green is now yellow
leds:blue is now green and not a heartbeat anymore
leds:ZigBee_RESET is added
leds:ZigBee_Ctrl is added
leds:WiFI_CHIP_En is added
leds:RFSW_Ctrl_1 is added
leds:RFSW_Ctrl_2 is added
leds:WiFi_RESET is added
leds:WiFi_Ctrl is added
leds:GSM_Ctrl is added
leds:UV_ENABLE is added
leds:FAN0 is added
```

Some difference appear to come of course having different device tree compiler
versions (linux,phandle addition for example) and number juggling.

Interestingly enough, here the first typo's and mistakes also start to appear.
I guess the portia team is small and doesn't do reviews ;) You'd think that when
doing git add -p, one would notice a:
```diff
-	pinctrl-names = "default";
+	pinctrl-names = "dsefault";	
```
or similarly that a MAC address was just committed:
```diff
+	local-mac-address = [00 27 02 11 a7 6c];	
```
Everybody uses git add -p right? And extra whitespaces ... criminal ;)

Also, the LED definition has been brilliantly reused from the Xplained board. It
is so obvious, that they renamed the name (phandle) but NOT the actual label.
I'm sure their code will read something like 'echo $green_state" > /sys/class/leds/blue/brightness'.
```
yellow {
	label = "green";
};
green {
	label = "blue";
};
```
 A final giggle was to see various control signals under the leds. I guess it
felt that it would make sense to abuse the led framework to toggle a GPIO.
In their defense, there currently is no way to pre-export a gpio as a GPIO.
And while things like wifi reset are normally properties of the wifi drivers,
that would require driver work if it is not yet available. Not to mention GSM
or ZigBee modules.

The major differences between the sama5d2_d1186b and sama5d2_d1186ba seem to be

|sama5d2_d1186b         | sama5d2_d1186ba       |
|-----------------------|-----------------------|
|serial1 = uart3	| serial1 = flx1:uart	|
|			| serial6 = flx3:uart	|
|			| sram			|
|ohci			|			|
|			| flx1			|
|uart3			|			|
|			| flx3 with uart	|
|adtrg			|			|
|charger_chglev		|			|
|charger_irq		|			|
|charger_lbo		|			|
|classd_default		|			|
|			| flx1			|
|			| flx3			|
|pinctrl_lcd_base	|			|
|qspi0_default		|			|
|uart3			|			|
|usb_default		|			|
|usb_vbus		|			|
|			| leds:GSM_Vcc		|
|			| leds:GSM_Pwr		|
|			| leds:GSM_Reset	|
|			| leds:CS_FL		|
|			| leds:POR_CTRL		|

## rootfs analysis
The rootfs.ubifs file contains a linux ubi based filesystem.
Extracting it using [Jason Pruitt](http://mechcalc.com/) [UBI Reader](https://github.com/jrspruitt/ubi_reader.git)
we find a [build-root](ubifs-root/etc/os-release) based system.
```
NAME=Buildroot
VERSION=2016.05-00012-gc32efa8
ID=buildroot
VERSION_ID=2016.05
PRETTY_NAME="Buildroot 2016.05"
```
Sadly, the execute permissions got lost and not all /dev entries remain. The
permissions in the usual suspects, [/usr]/[s]bin have been fixed.

It has busbox as its base and run various interesting services during [boot](ubifs-root/etc/inittab).
From here it seems dropbear is started by default. As my inverter runs a year
old version (3.2016) and doesn't respond to anything. Maybe after a firmware
update these features become available to me.

With ssh login, there should be some interesting finds in [/etc/shadow](ubifs-root/etc/shadow). After a
few seconds with John the ripper, we find that the user 'root' has as 'a' as
its password. Yes, just the single letter 'a'. I have not yet found if this
is overwritten at some point with the local data on the inverter or not but
that seems unlikely.

The image itself seems reasonably stock build-root with the main inverter task
residing in the core_app in [/root](ubifs-root/root). Further more the
web-application runs from [/www](ubifs-root/www) which is written in python-cgi. The webserver
has [admin:admin](ubifs-root/etc/lighttpd/httppwd) as its default credentials
and talks to the pmanager python service. Looking in /usr/lib/python3.4 we see
many buildroot installed python standard library as py caches. The more juicy
bits seem to reside in [site-packages](ubifs-root/usr/lib/python3.4/site-packages) including their own python application!

Looking over these files it appears that the pmanager main applet talks to a
unix socket to likely the core_app.

A final note, some residual files seem to have been abandoned here as well.
[known_hosts](ubifs-root/root/.ssh/known_hosts) appears to hold some ssh host keys.
```
194.31.58.76 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHFNTgLHz3CujQCZQn5arOz2/MBZZT4A3RiQx7W6sjjCsflM+NOH/ib3Hge5izHIthpgAv1Ejc0PvIacORU/l7o=
172.20.101.224 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHFNTgLHz3CujQCZQn5arOz2/MBZZT4A3RiQx7W6sjjCsflM+NOH/ib3Hge5izHIthpgAv1Ejc0PvIacORU/l7o=
linux-prod.solaredge.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHFNTgLHz3CujQCZQn5arOz2/MBZZT4A3RiQx7W6sjjCsflM+NOH/ib3Hge5izHIthpgAv1Ejc0PvIacORU/l7o=
rndsshtest.solaredge.local ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHFNTgLHz3CujQCZQn5arOz2/MBZZT4A3RiQx7W6sjjCsflM+NOH/ib3Hge5izHIthpgAv1Ejc0PvIacORU/l7o=
```

### core_app
The main application of the inverter that handles the UI, connections and
protocols etc. If qemu-user-static is installed, the rootfs can be chrooted into.
Launching the core_app gives some info and just loops forever with 100% cpu usage.
Maybe waiting a long time makes it do something, but this is the output received
after startup.
```
Unsupported ioctl: cmd=0x542f
Unsupported ioctl: cmd=0x542f
Unsupported ioctl: cmd=0x542f
~~~~~~~~~ SET VALID = FALSE in init
SAVING IN_CONFIGS FILE SIZE OF: 576
FILE WRITE WAS: 1
DSPConfigMngr.DSPConfig.configs_valid == 0
```

It also creates a bunch of binary files in [/usr/preserved/stats](ubifs-root/usr/preserved/stats).

### sepython-pmanager
Decompiled sources have now replaced the python caches using [uncompyle6](https://github.com/rocky/python-uncompyle6) leaving
plenty of room for analysis.
