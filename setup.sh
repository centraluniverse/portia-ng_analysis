#!/bin/sh

# This script contains pre-install and post-install functions to be run
# by SWupdate before and after the upgrade process.

LOGFILE="/root/swupgrade.log"
TARGET=0

log()	{ echo $@ >> $LOGFILE; }

findtarget () {
	if grep -q "root=ubi0:rootfs1" /proc/cmdline; then
		TARGET=2
	elif grep -q "root=ubi0:rootfs2" /proc/cmdline; then
		TARGET=1
	else
		log "Could not find UBI volume"
		exit 1
	fi
}

ubootcopy1 () {
	fw_setenv bootargs "console=ttyS0,115200 mtdparts=spi32766.0:64k(at91bootstrap)ro,64k(bootloaderenv),384k(uboot)ro,64k(dtb1),64k(dtb2),4288k(kernel1),4288k(kernel2),-(rootfs) rootfstype=ubifs ubi.mtd=7 root=ubi0:rootfs1 rw"
	fw_setenv bootargsbackup "setenv bootargs console=ttyS0,115200 mtdparts=spi32766.0:64k(at91bootstrap)ro,64k(bootloaderenv),384k(uboot)ro,64k(dtb1),64k(dtb2),4288k(kernel1),4288k(kernel2),-(rootfs) rootfstype=ubifs ubi.mtd=7 root=ubi0:rootfs2 rw"
	fw_setenv bootcmd "gpio set 59; gpio set 79; sf probe 0; sf read 0x21000000 0x00080000 0x7aa9; sf read 0x22000000 0x000A0000 0x430000; bootz 0x22000000 - 0x21000000"
	fw_setenv bootcmdbackup "setenv bootcmd gpio set 59\\\\; gpio set 79\\\\; sf probe 0\\\\; sf read 0x21000000 0x00090000 0x7aa9\\\\; sf read 0x22000000 0x004D0000 0x430000\\\\; bootz 0x22000000 - 0x21000000"
	fw_setenv altbootcmd "run bootargsbackup; run bootcmdbackup; saveenv; boot"
	fw_setenv mtdids "nor0=spi32766.0"
	fw_setenv bootcount "0"
	fw_setenv bootlimit "3"
	fw_setenv upgrade_available "1"
}

ubootcopy2 () {
	fw_setenv bootargs "console=ttyS0,115200 mtdparts=spi32766.0:64k(at91bootstrap)ro,64k(bootloaderenv),384k(uboot)ro,64k(dtb1),64k(dtb2),4288k(kernel1),4288k(kernel2),-(rootfs) rootfstype=ubifs ubi.mtd=7 root=ubi0:rootfs2 rw"
	fw_setenv bootargsbackup "setenv bootargs console=ttyS0,115200 mtdparts=spi32766.0:64k(at91bootstrap)ro,64k(bootloaderenv),384k(uboot)ro,64k(dtb1),64k(dtb2),4288k(kernel1),4288k(kernel2),-(rootfs) rootfstype=ubifs ubi.mtd=7 root=ubi0:rootfs1 rw"
	fw_setenv bootcmd "gpio set 59; gpio set 79; sf probe 0; sf read 0x21000000 0x00090000 0x7aa9; sf read 0x22000000 0x004D0000 0x430000; bootz 0x22000000 - 0x21000000"
	fw_setenv bootcmdbackup "setenv bootcmd gpio set 59\\\\; gpio set 79\\\\; sf probe 0\\\\; sf read 0x21000000 0x00080000 0x7aa9\\\\; sf read 0x22000000 0x000A0000 0x430000\\\\; bootz 0x22000000 - 0x21000000"
	fw_setenv altbootcmd "run bootargsbackup; run bootcmdbackup; saveenv; boot"
	fw_setenv mtdids "nor0=spi32766.0"
	fw_setenv bootcount "0"
	fw_setenv bootlimit "3"
	fw_setenv upgrade_available "1"
}

preinstall () {
	log "preinst()"
}

postinstall () {
	log "postinst()"

	findtarget

	if [ "$TARGET" == 1 ]; then
		ubootcopy1
	elif [ "$TARGET" == 2 ]; then
		ubootcopy2
	fi
}

case $1 in
    preinst)
        preinstall
        ;;
    postinst)
        postinstall
        ;;
    *)
        echo "Upgrade configuration script"
        echo "Please specify preinstall or postinstall"
        ;;
esac

exit 0
