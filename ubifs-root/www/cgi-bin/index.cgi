#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: config.cgi
## $Revision:: 21561             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-01-26 20:13:53 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: send configurations to pmanager
##
##
##############################################################################

import cgi, os
import cgitb; cgitb.enable()

##############################################################################

message =  "Loading... please wait, this may take up to 30 seconds"
redirect = "/cgi-bin/main.cgi"

print """\
Content-Type: text/html\n
<html><head>
<meta http-equiv="refresh" content="0;url=%s" />
<title></title>
</head><body>
<img src="/solaredge.png" width="301" height="62">
<h1>
	Gateway Configuration</h1>
<hr />
<p>%s</p>
</body></html>
""" % (redirect, message,)
