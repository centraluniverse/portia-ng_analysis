#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: config.cgi
## $Revision:: 21561             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-01-26 20:13:53 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: send configurations to pmanager
##
##
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import socket
import env
import msg_queue
import time
from datetime import datetime

s = env.PMANAGER_SOCK_2
sd_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")
timer = time.time()

##############################################################################

try:
	sd_queue.web_req_slave_detect_status()	
	resp = sd_queue.recv(60)
	sd_queue.close()
	meta = """<meta http-equiv="refresh" content="5;url=slave_detect_progress.cgi" />"""

	dt = datetime.now().strftime('%H:%M:%S')

	if 'Not Active' in resp['status']:
		line1 = "<p>Done!</p><p></p>"
		line2 = "<b><p>" + str(resp['slaves num']) + " Slaves were detected</b></p><p></p>"
		meta = ''
	else:
		line1 = "<p>Slave Detect in progress... Please do not close this window, the process will take about 3 minutes (Last update time: " +"<b>" + dt + "</b>" + ")</p><p></p>"
		line2 = "<p><b>" + str(resp['slaves num']) + " Slaves were detected until now</b></p><p></p>"
		
	message = line1 + line2

	if ((resp is None) or (resp['name'] != "web resp slave detect status")):
		raise Exception("No response from pmanager")
except:
	#cgi.print_exception()
	message = 'Slave Detect Failed'
	sd_queue.close()



print """\
Content-Type: text/html\n
<html><head>
%s
<title></title>
</head><body>
<img src="/solaredge.png" width="301" height="62">
<h1>
	Gateway Configuration</h1>
<hr />
<p>%s</p>
<hr />
<form action="/cgi-bin/index.cgi" method="post">
<h2>
	<input name="to_index" type="submit" value="Return to Main Page" id="return_to_index" /></h2>
</form>
</body></html>
""" % (meta, message,)
