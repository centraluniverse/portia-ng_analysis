#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: index.cgi
## $Revision:: 21940             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-03-06 14:42:33 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: Generates the default HTML file that appears in a browser 
##
##		
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import env
import socket
import json
import msg_queue
import logger
import time # remove

color_err = '#ff0000'
color_ok = '#008000'

s = env.PMANAGER_SOCK_2

##############################################################################

def status():
	# Send to pmanager the Upgarde command
	index_queue.web_req_status()
	resp = index_queue.recv(60)
	index_queue.close()
	
	if ((resp is None) or (resp['name'] != "web resp status")):
		raise Exception("No response from pmanager")

	return resp

##############################################################################

# Get Status from pmanager
try:
	index_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")
	resp = status()
except:
	#cgi.print_exception()
	message = "Error Loading Page"
	print """\
	Content-Type: text/html\n
	<html><body>
	<img src="/solaredge.png" width="301" height="62">
	<h1>
		Gateway Configuration</h1>
	<hr />
	<p>%s</p>
	<hr />
	<form action="/cgi-bin/index.cgi" method="post">
	<h2>
		<input name="to_index" type="submit" value="Return to Main Page" id="return_to_index" /></h2>
	</form>
	</body>
	</html>
	""" % (message,)
	exit()


# Update strings for html generation
status = {}
config = {}

appver = resp['app ver'] 
sn = resp['sn'] 
systime = resp['systime'] 

status['lan'] 		= {}
status['internet']	= {}
status['inverters'] = {}
status['lbox'] 		= {}
status['server'] 	= {}

status['lan']['status']			= 'OK' if 'OK' in resp['status lan'] 		 else 'Error'
status['internet']['status'] 	= 'OK' if 'OK' in resp['status internet'] 	 else 'Error'
status['inverters']['status'] 	= 'OK' if 'OK' in resp['status inverters'] 	 else 'Error'
status['lbox']['status'] 		= 'OK' if 'OK' in resp['status lbox'] 		 else 'Error'
status['server']['status'] 		= 'OK' if 'OK' in resp['status server'] 	 else 'Error'

status['lan']['color']			= color_ok if 'OK' in status['lan']['status']  		else color_err
status['internet']['color'] 	= color_ok if 'OK' in status['internet']['status'] 	else color_err
status['inverters']['color'] 	= color_ok if 'OK' in status['inverters']['status'] else color_err
status['lbox']['color'] 		= color_ok if 'OK' in status['lbox']['status'] 		else color_err
status['server']['color'] 		= color_ok if 'OK' in status['server']['status']	else color_err

status['lan']['info']			= ''
status['internet']['info'] 		= ''
status['inverters']['info'] 	= resp['inverters data']
status['lbox']['info'] 			= '' if 'Error' in status['lbox']['status'] else resp['lbox data']
status['server']['info'] 		= ''

config['dhcp'] 		= resp['dhcp']
config['ipver'] 	= resp['ipver']
config['ipaddr'] 	= resp['ipaddr']
config['netmask'] 	= resp['netmask']
config['gateway'] 	= resp['gateway']
config['dns1'] 		= resp['dns1']
config['dns2'] 		= resp['dns2']
config['lbox'] 		= resp['lbox']
config['ntp'] 		= resp['ntp']

print "Content-type:text/html\r\n\r\n"
html = """
<html>
	<head>
		<title></title>
	</head>
	<body>
		<img src="/solaredge.png" width="301" height="62">
		<h1>
			Gateway Configuration</h1>
		<hr />
		<h2>
			Information</h2>
		<p>
			<b>App version:</b> %s</p>
		<p>
			<b>Gateway SN:</b> %s</p>
		<p>
			<b>System Time:</b> %s</p>
		<h2>
			Status</h2>
		<table border="1" cellpadding="1" cellspacing="1" style="width: 450px">
			<tbody>
				<tr>
					<td>
						&nbsp;</td>
					<td>
						Status</td>
					<td>
						Info</td>
				</tr>
				<tr>
					<td>
						LAN</td>
					<td>
						<span style="color:%s;"><strong>%s</strong></span></td>
					<td>
						%s</td>
				</tr>
				<tr>
					<td>
						Internet</td>
					<td>
						<strong style="color:%s;">%s</strong></td>
					<td>
						%s</td>
				</tr>
				<tr>
					<td>
						Inverters</td>
					<td>
						<strong><span style="color:%s;">%s</span></strong></td>
					<td>
						%s</td>
				</tr>
				<tr>
					<td>
						L-Box</td>
					<td>
						<strong style="color:%s;">%s</strong></td>
					<td>
						%s</td>
				</tr>
				<tr>
					<td>
						Server</td>
					<td>
						<strong style="color:%s;">%s</strong></td>
					<td>
						%s</td>
				</tr>
			</tbody>
		</table>
		
		<form action="/cgi-bin/config.cgi" method="post">

		<h2>
			Configurations</h2>
		<h3>
			Lan Configuration</h3>
		<p>
			DHCP Client:&nbsp;<input %s name="dhcp" type="radio" value="TRUE" />Enable&nbsp;<input %s name="dhcp" type="radio" value="FALSE" />Disable&nbsp;</p>
		<p>
			IP Version:&nbsp;<input %s name="ipver" type="radio" value="4" />IPv4&nbsp;<input %s name="ipver" type="hidden" value="6"/></p>
		<p>
			Static IP Configuration:</p>
		<p>
			&nbsp; &nbsp;IP:&nbsp;<input maxlength="15" name="ipaddr" type="text" value="%s" /></p>
		<p>
			&nbsp; &nbsp;Netmask:&nbsp;<input maxlength="15" name="netmask" type="text" value="%s" /></p>
		<p>
			&nbsp; &nbsp;Gateway:&nbsp;<input maxlength="15" name="gateway" type="text" value="%s" /></p>
		<p>
			&nbsp; &nbsp;DNS1:&nbsp;<input maxlength="15" name="dns1" type="text" value="%s" /></p>
		<p>
			&nbsp; &nbsp;DNS2:&nbsp;<input maxlength="15" name="dns2" type="text" value="%s" /></p>
		<h3>
			L-Box Configuration</h3>
		<p>
			URL:&nbsp;<input name="lbox" size="30" type="text" value="%s" /></p>
		<h3>
			NTP Configuration</h3>
		<p>
			URL:&nbsp;<input name="ntp" size="30" type="text" value="%s" /></p>
		<h2>
			<input name="conf_save" type="submit" value="Save Configuration" id="btn_s" /></h2>
		<hr />
	
		<h2>
			Factory Reset</h2>
		<h2>
			<input name="conf_restore" type="submit" value="Reset" id="btn_r" /></h2>
		<hr />
		</form>

		<h2>
			Software Upgrade</h2>
		<form enctype="multipart/form-data" action="/cgi-bin/upload.cgi" method="POST">
		<p>
			<input name="file" type="file" value="Choose File" />&nbsp;</p>
			<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
		<p>
			<input name="sw_perform" type="submit" value="Perform Upgrade" /></p>
		<hr />
				
		</form>

		<h2>
			RS-485 Slave Detect</h2>
		<form action="/cgi-bin/slave_detect.cgi" method="post">
			<input name="slave_detect_start" type="submit" value="Start" /></p>
		<p>

			&nbsp;</p>
		<p>
			&nbsp;</p>
		<p>
			&nbsp;</p>
		<div>
			&nbsp;</div>
		</form>
	</body>
</html>
"""

print html % (  appver, sn, systime,
				status['lan']['color'],			status['lan']['status'], 		status['lan']['info'],	
				status['internet']['color'], 	status['internet']['status'], 	status['internet']['info'], 
				status['inverters']['color'], 	status['inverters']['status'], 	status['inverters']['info'],  
				status['lbox']['color'], 		status['lbox']['status'], 		status['lbox']['info'],	
				status['server']['color'], 		status['server']['status'], 	status['server']['info'], 	
				'checked' if "Enable" in config['dhcp'] else '',
				'checked' if "Enable" not in config['dhcp'] else '',		
				'checked' if '6' not in config['ipver'] else '',
				'checked' if '6' in config['ipver'] else '',	
				config['ipaddr'], 		
				config['netmask'], 	
				config['gateway'], 	
				config['dns1'], 		
				config['dns2'], 		
				config['lbox'],
				config['ntp']) 	




