#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: config.cgi
## $Revision:: 21939             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-03-06 14:09:39 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: send configurations to pmanager
##
##
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import socket
import env
import json
import msg_queue
from IPy import IP

s = env.PMANAGER_SOCK_2
config_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")
config = {}

##############################################################################

def input_validation():
	# check if all IP's valid
	try:
		IP(config['ipaddr'])
		IP(config['netmask'])
		IP(config['gateway'])
	
		# Check if all IP's in the correct version
		if (IP(config['ipaddr']).version() != int(config['ipver']) or
			IP(config['netmask']).version() != int(config['ipver']) or
			IP(config['gateway']).version() != int(config['ipver'])):
			return False
		
		if (config['dns1'] != ''):
			IP(config['dns1'])
			if (IP(config['dns1']).version() != int(config['ipver'])):
				return False;	

		if (config['dns2'] != ''):
			IP(config['dns2'])
			if (IP(config['dns2']).version() != int(config['ipver'])):
				return False;	
	except:
		return False

	return True

##############################################################################

try:
	# Create instance of FieldStorage 
	form = cgi.FieldStorage() 

	if 'conf_save' in form:
		# Get data from fields
		config['dhcp'] 		= form.getvalue('dhcp')
		config['ipver'] 	= '4' #form.getvalue('ipver') # Not supported Ipv6
		config['ipaddr'] 	= form.getvalue('ipaddr')
		config['netmask'] 	= form.getvalue('netmask')
		config['gateway'] 	= form.getvalue('gateway')
		config['dns1'] 		= '' if form.getvalue('dns1') is None else form.getvalue('dns1')
		config['dns2'] 		= '' if form.getvalue('dns2') is None else form.getvalue('dns2')
		config['lbox']		= '' if form.getvalue('lbox') is None else form.getvalue('lbox')
		config['ntp']		= '' if form.getvalue('ntp') is None else form.getvalue('ntp')

		if (('FALSE' in config['dhcp']) and (input_validation() == False)):
			raise Exception("Bad Inputs: " + str(json.dumps(config)))

		config_queue.web_cmd_configs(config)	
	else: # 'conf_reset' in form:
		config_queue.web_cmd_configs_reset()	
	
	resp = config_queue.recv(60)
	config_queue.close()
	
	if ((resp is None) or (resp['name'] != "web resp success")):
		raise Exception("No response from pmanager")
except:
	#cgi.print_exception()
	message = 'Failed to set configurations'
	config_queue.close()
else:
	message = 'Gateway was successfully configured'


print """\
Content-Type: text/html\n
<html><body>
<img src="/solaredge.png" width="301" height="62">
<h1>
	Gateway Configuration</h1>
<hr />
<p>%s</p>
<hr />
<form action="/cgi-bin/index.cgi" method="post">
<h2>
	<input name="to_index" type="submit" value="Return to Main Page" id="return_to_index" /></h2>
</form>
</body>
</html>
""" % (message,)
