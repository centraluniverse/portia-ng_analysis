#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: config.cgi
## $Revision:: 21561             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-01-26 20:13:53 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: send configurations to pmanager
##
##
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import socket
import env
import msg_queue
import time

s = env.PMANAGER_SOCK_2
sd_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")
timer = time.time()

##############################################################################

try:
	sd_queue.web_cmd_slave_detect()	
	resp = sd_queue.recv(60)
	sd_queue.close()

	message =  "Starting Slave Detect..."
	redirect = "slave_detect_progress.cgi"

	if ((resp is None) or (resp['name'] != "web resp success")):
		raise Exception("No response from pmanager")
except:
	#cgi.print_exception()
	message = 'Slave Detect Failed'
	sd_queue.close()



print """\
Content-Type: text/html\n
<html><head>
<meta http-equiv="refresh" content="0;url=%s" />
<title></title>
</head><body>
<img src="/solaredge.png" width="301" height="62">
<h1>
	Gateway Configuration</h1>
<hr />
<p>%s</p>
</body></html>
""" % (redirect, message,)
