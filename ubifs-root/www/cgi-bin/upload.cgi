#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: upload.cgi
## $Revision:: 21939             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-03-06 14:09:39 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: 
## The purpose is to store the upgrade file into temp. location in the
## WEB FS and pass the file path to pmanager that will handle the upgrade process.
## Executed by http web server.
##
## Interface with pmanager described in pmanager.py
##
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import socket
import time
import env
import sys
import msg_queue

f = 'files/'
s = env.PMANAGER_SOCK_2
upload_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")

form = cgi.FieldStorage()

# A nested FieldStorage instance holds the file
fileitem = form['file']

try:
	# remove files from directory
	os.popen('rm -f ' + f + '*')

	# strip leading path from file name to avoid directory traversal attacks
	fn = os.path.basename(fileitem.filename)
	open(f + fn, 'wb').write(fileitem.file.read())

	# Send to pmanager the Upgarde command
	upload_queue.web_cmd_upgrade(fn)
	resp = upload_queue.recv(60)
	upload_queue.close()
	
	if ((resp is None) or (resp['name'] != "web resp success")):
		raise Exception("No response from pmanager")
		
except:
	#cgi.print_exception()
	message = 'Upgrade failed'
	upload_queue.close()
else:
	message = """\
	Upload Success %s
	Please wait upto 2 minutes for system to upgrade and reboot...
	""" % (fn,)

print """\
Content-Type: text/html\n
<html><body>
<img src="/solaredge.png" width="301" height="62">
<h1>
	Gateway Configuration</h1>
<hr />
<p>%s</p>
<hr />
<form action="/cgi-bin/index.cgi" method="post">
<h2>
	<input name="to_index" type="submit" value="Return to Main Page" id="return_to_index" /></h2>
</form>
</body></html>
""" % (message,)

