#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: config_developer.cgi
## $Revision:: 21848             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-02-26 12:55:16 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: send configurations to pmanager
##
##
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import socket
import env
import json
import msg_queue
from IPy import IP

s = env.PMANAGER_SOCK_2
config_d_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")
config_d = {}

##############################################################################

try:
	# Create instance of FieldStorage 
	form = cgi.FieldStorage() 

	if 'conf_d_save' in form:
		# Get data from fields
		config_d['sn'] 		= form.getvalue('sn')
		config_d['listen port'] = form.getvalue('listen_port')
		
		config_d_queue.web_cmd_configs_developer(config_d)	
	else: # 'conf_reset' in form:
		config_d_queue.web_cmd_configs_developer_reset()	
	
	resp = config_d_queue.recv(60)
	config_d_queue.close()
	
	if ((resp is None) or (resp['name'] != "web resp success")):
		raise Exception("No response from pmanager")
except:
	cgi.print_exception()
	message = 'Failed to set configurations'
	config_d_queue.close()
else:
	message = 'Gateway was successfully configured'


print """\
Content-Type: text/html\n
<html><body>
<img src="../solaredge.png" width="301" height="62">
<h1>
	Gateway Configuration</h1>
<hr />
<p>%s</p>
</body></html>
""" % (message,)
