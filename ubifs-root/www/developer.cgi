#!/usr/bin/env python
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: developer.cgi
## $Revision:: 21939             $:  Revision of last commit
## $Author:: yuval.g             $:  Author of last commit
## $Date:: 2017-03-06 14:09:39 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: Generates the default HTML file that appears in a browser 
##
##		
##############################################################################

import cgi, os
import cgitb; cgitb.enable()
import env
import socket
import json
import msg_queue
import logger
import time # remove

s = env.PMANAGER_SOCK_2
developer_queue = msg_queue.Prot(env.PMANAGER_SOCK_2, "client")

##############################################################################

def status_developer():
	developer_queue.web_req_status_developer()
	resp = developer_queue.recv(60)
	developer_queue.close()
	
	if ((resp is None) or (resp['name'] != "web resp status developer")):
		raise Exception("No response from pmanager")

	return resp

##############################################################################

# Get Status from pmanager
try:
	resp = status_developer()
except:
	developer_queue.close()
	cgi.print_exception()

# Update strings for html generation
status_d = {}

status_d['sn'] 		= resp['sn']
status_d['listen port'] = resp['listen port']

print "Content-type:text/html\r\n\r\n"
html = """
<html>
	<head>
		<title></title>
	</head>
	<body>
		<img src="/solaredge.png" width="301" height="62">
		<h1>
			Gateway Configuration - R&D only </h1>
		<hr />
		
		<form action="/cgi-bin/config_developer.cgi" method="post">

		<h2>
			General Configuration</h2>
		<p>
			&nbsp; &nbsp;SN:&nbsp;<input maxlength="15" name="sn" type="text" value="%s" /></p>

		<h2>
			TCP Configuration</h2>
		<p>
			&nbsp; &nbsp;TCP Server Listening Port:&nbsp;<input maxlength="15" name="listen_port" type="text" value="%s" /></p>
		<hr />
		<h2>
			<input name="conf_d_save" type="submit" value="Save Configuration" id="btn_d_s" />&nbsp;<input name="conf_d_restore" type="submit" value="Restore Defaults" id="btn_d_r" /></h2>
		<hr />

		</form>

		<p>
			&nbsp;</p>
		<p>
			&nbsp;</p>
		<p>
			&nbsp;</p>
		<div>
			&nbsp;</div>
	</body>
</html>
"""

print html % (status_d['sn'], status_d['listen port']) 	




