#!/bin/sh

LOGFILE="/root/portia.log"

log()   { echo "$(date). PowerLog.sh: "$@ >> $LOGFILE; }

log $*

exit 0
