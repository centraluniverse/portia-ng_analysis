#!/bin/sh

# This script automatically upgrades the system using a USB device
                          
file="/var/log/messages"  
LOGFILE="/root/portia.log"
                                             
log ()  {                                    
        echo "usb-detect.sh: " $@ >> $LOGFILE
        echo "usb-detect.sh: " $@
}        
                  
main () {         
        log `date`                                                        
                                                                          
        tail -n 50 -f $file | while read -r f1 f2 f3 f4 f5 f6 f7 f8 f9 f10
        do                                                                 
                if [ "$f10" = "Attached SCSI removable disk" ]; then       
                        device=`echo $f9 | cut -d "[" -f2 | cut -d "]" -f1`
                        device=/dev/$device
                        dest=`mktemp -d`                                     
                                                                             
                        log "Found USB device $device, attempting to mount.."
                        mount $device $dest                              
                                                                         
                        log "Upgrading with files: " `ls $dest/swupdate/`
                        # python3 sw_upgrade $dest/swupdate/
                                    
                        umount $dest
                fi
        done
 
}   
       
main   
exit $?

