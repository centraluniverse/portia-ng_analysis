#!/bin/sh

usage()
{
	echo  "----------------------------------------------------------------------------------"
	echo  " Usage:"
	echo  "	This script checks how many instances of the given app <app_name> running (according"
	echo  "	to ps output)"
	echo  "	equal - check if exact number <number_of_runs> of instances"
	echo  "	greater - check if more than <number_of_runs> instances"
	echo  "	"
	echo  "		$0 <equal|greater> <number_of_runs> <app_name>"
	echo  "	e.g."
	echo  "	        ./checkapp.sh greater 1 core_app"
	echo  "	"
	echo  "----------------------------------------------------------------------------------"
}

main()
{
	if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
		usage
		exit 1
	fi

	num=`ps -o comm | grep $3 | grep / | wc -l`

	case "$1" in
                equal)
                        if [ $num -eq $2 ]; then
				echo "true"
				exit 0
			fi
                        ;;
                greater)
                        if [ $num -gt $2 ]; then
				echo "true"
				exit 0
			fi
                        ;;
                *)
                        usage
                        ;;
	esac

	echo "false"
	exit 0
}	

main $*
exit 0
