#!/bin/sh

# This script handles various tasks at system startup
# log="/root/startup.log"

set_sym_links () {
        if [ ! -e  /www/cgi-bin/files ]; then
                ln -s /data/files /www/cgi-bin/files
                echo "added symbolic link /www/cgi-bin/files"
        fi

        if [ ! -e  /usr/preserved/telems ]; then
                ln -s /data/telems /usr/preserved/telems
                echo "added symbolic link /usr/preserved/telems"
        fi

        if [ ! -e  /usr/preserved/configs ]; then
                ln -s /data/configs /usr/preserved/configs
                echo "added symbolic link /usr/preserved/configs"
        fi

        if [ ! -e  /usr/preserved/stats ]; then
                ln -s /data/stats /usr/preserved/stats
                echo "added symbolic link /usr/preserved/stats"
        fi

        if [ ! -e  /usr/preserved/user ]; then
                ln -s /data/user /usr/preserved/user
                echo "added symbolic link /usr/preserved/user"
        fi

        if [ ! -e  /usr/preserved/dsp1 ]; then
                ln -s /data/dsp1 /usr/preserved/dsp1
                echo "added symbolic link /usr/preserved/dsp1"
        fi

        if [ ! -e  /usr/preserved/dsp2 ]; then
                ln -s /data/dsp2 /usr/preserved/dsp2
                echo "added symbolic link /usr/preserved/dsp2"
        fi
}

check_wilc_hostapd_wpa_file () {
        if [ -f /etc/wilc_hostapd_wpa.conf ] && [ ! -f /data/configs/wilc_hostapd_wpa.conf ]; then
                echo cp /etc/wilc_hostapd_wpa.conf /data/configs/
                cp /etc/wilc_hostapd_wpa.conf /data/configs/
        fi
}

check_magic () {
        if [ -f /etc/certs/magic ] && [ ! -f /data/configs/magic ]; then
                echo cp /etc/certs/magic /data/configs/
                cp /etc/certs/magic /data/configs/
        fi
}

set_wifi_ssid () {
        if [ -f /data/configs/wilc_hostapd_wpa.conf ] && [ -f /usr/preserved/configs/core_app_config ] && [ ! -f /usr/preserved/configs/wifi_remotely_configured ]; then
                id=`awk '/"name":\t"ID"/,/"value"/' /usr/preserved/configs/core_app_config  | grep value | cut -f 5 | cut -d, -f 1`
                hexid=`printf "%X" $id`
                checksum="00"
                sed -i -e "s/\(ssid *= *\).*/ssid=SEDG-$hexid-$checksum/" /data/configs/wilc_hostapd_wpa.conf  
        fi
}

set_bootenv () {
        uboot_env=`fw_printenv`

        # If we just finished upgrading and have successfully booted then
        # change the upgrade_available variable to 0.
        flag=`echo "$uboot_env" | grep "upgrade_available=1"`
        if [ ! -z "$flag" ]; then
                # echo `date` >> $log
                # echo "Upgrade successful.. Updating U-Boot variables." >> $log
                fw_setenv upgrade_available "0"
                fw_setenv bootcount "1"
        fi

        # Update DTB name and version to /etc/sw-versions
        dtb_name=`echo "$uboot_env" | grep dtb_name | cut -d= -f 2`
        if [ ! -z "$dtb_name" ]; then
                data="dtb                 $dtb_name"
                sed -i "s/^dtb.*/${data}/" /etc/sw-versions
        fi

}

run_udhcpc() {
        ip=`/root/netconfig.sh get ipaddr`
        if [ -z "$ip" ]; then
                udhcpc -t 0 -T 5 -b > /dev/null &
        fi
}


check_interfaces_file () {
        if [ -f /etc/network/interfaces ] && [ ! -f /data/configs/interfaces ]; then
                echo "cp /etc/network/interfaces /data/configs/"
                cp /etc/network/interfaces /data/configs/
        fi
        if [ ! -f /etc/network/interfaces ] && [ -f /data/configs/interfaces ]; then
                echo "cp /data/configs/interfaces /etc/network/"
                cp /data/configs/interfaces /etc/network/
        fi

        if [ -f /etc/resolv.conf ] && [ ! -f /data/configs/resolv.conf ]; then
                echo "cp /etc/resolv.conf /data/configs/"
                cp /etc/resolv.conf /data/configs/
        fi
        if [ ! -f /etc/resolv.conf ] && [ -f /data/configs/resolv.conf ]; then
                echo "cp /data/configs/resolv.conf /etc/"
                cp /data/configs/resolv.conf /etc/
        fi

        diff1=`diff /etc/network/interfaces /data/configs/interfaces`
        diff2=`diff /etc/resolv.conf /data/configs/resolv.conf`
        if [ ! -z "$diff1" ] || [ ! -z "$diff2" ]; then
                echo "cp /data/configs/interfaces /etc/network/"
                cp /data/configs/interfaces /etc/network/
                echo "cp /data/configs/resolv.conf /etc/"
                cp /data/configs/resolv.conf /etc/
                echo "ifconfig eth0 down"
                ifconfig eth0 down
                echo "ifconfig eth0 up"
                ifconfig eth0 up
        fi
}

check_params_file() {
    if [ -f /etc/params ] && [ ! -f /data/configs/params ]; then
        echo cp /etc/params /data/configs/params
        cp /etc/params /data/configs/params
    fi
}

check_rest_api_settings() {
    if [ -f /etc/config/api_settings.json ] && [ ! -f /data/configs/api_settings.json ]; then
        echo cp /etc/config/api_settings.json /data/configs/api_settings.json 
        cp /etc/config/api_settings.json /data/configs/api_settings.json 
    fi
}

path_create(){
    if [ ! -d /data/spff ]; then
        mkdir /data/spff -p 
    fi
}

main () {
        set_sym_links
        check_interfaces_file
        check_wilc_hostapd_wpa_file        
        check_magic
        check_params_file
        set_bootenv
        set_wifi_ssid
        run_udhcpc
        check_rest_api_settings
        path_create
}

main

exit 0

