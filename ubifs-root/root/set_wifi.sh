#!/bin/sh


usage()
{
	echo -e "-----------------------------------------------------"
	echo -e "Usage:"
	echo -e "\set_wifi set ssid password security_type"
    echo -e "\set_wifi get <ssid | security_type | password | mac>"
	echo -e "-----------------------------------------------------"
}

wilc_conf_file="/data/configs/wilc_hostapd_wpa.conf"
wifi_configured_flag="/usr/preserved/configs/wifi_remotely_configured"

set_wifi() 
{
	if [ -f $wilc_conf_file ] ; then
        touch $wifi_configured_flag
		sed -i -e "s/\(ssid *= *\).*/ssid=$1/" $wilc_conf_file
        sed -i -e "s/\(wpa_passphrase *= *\).*/wpa_passphrase=$2/" $wilc_conf_file
        sed -i -e "s/\(wpa_key_mgmt *= *\).*/wpa_key_mgmt=$3/" $wilc_conf_file 
	fi
}


get_wifi()
{
    case "$1" in
        
        ssid)
                        grep "ssid=" $wilc_conf_file | cut -d'=' -f 2 
                        ;;
        security_type)
                        grep "wpa_key_mgmt=" $wilc_conf_file | cut -d'=' -f 2
                        ;;
        password)
                        grep "wpa_passphrase=" $wilc_conf_file | cut -d'=' -f 2
                        ;;
        mac)
                        ifconfig -a | grep ^wlan0 | cut -d ' ' -f 10
                        ;;
        *)
                        usage
                        ;;
    esac

}


main()
{

    case "$1" in
     
              set)
                      set_wifi "$2" "$3" "$4"
                      ;;
              get)
                      get_wifi "$2"
                      ;;
               *)
                      usage
                      ;;
    esac
}

main $*
exit 0


