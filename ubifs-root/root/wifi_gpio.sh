#!/bin/sh

if [ -z "$1" ]; then
        echo "usage wifi_gpio.sh <on|off>"
        exit 1
fi

if [ "$1" == "on" ]; then
        tmp=`fw_printenv | grep bootcmd | sed -e 's/clear /set / g' | cut -d'=' -f 2`
        fw_setenv bootcmd "$tmp"
elif [ "$1" == "off" ]; then
        tmp=`fw_printenv | grep bootcmd | sed -e 's/set /clear / g' | cut -d'=' -f 2`
        fw_setenv bootcmd "$tmp"                                             
else
        echo "usage wifi_gpio.sh <on|off>"
        exit 1                                                               
fi

