#!/bin/sh
##############################################################################
## COPYRIGHT (C) SolarEdge Inc. All rights reserved.
## 
##-----------------------------------------------------------------------------
## Filename: netconfig.sh
## $Revision:: 21936             $:  Revision of last commit
## $Author:: ben.s               $:  Author of last commit
## $Date:: 2017-03-06 11:42:48 +#$:  Date of last commit
##-----------------------------------------------------------------------------
## Desc: 
##
##
##############################################################################

usage()
{
        echo -e "-----------------------------------------------------"
        echo -e "Usage:"
        echo -e "\tget <ipaddr|ipaddr6|gateway|dns1|dns2|netmask|internet|lan>"
        echo -e "\tget hwaddr"        
        echo -e "\tset static <inet4|inet6> <ip> <netmask> <gateway>"
        echo -e "\tset dhcp   <inet4|inet6>"
        echo -e "\tset dns    [ip1] [ip2]"
        echo -e "\tset hwaddr <XX:XX:XX:XX:XX:XX>"
        echo -e "-----------------------------------------------------"
}

net_restart()   { /etc/init.d/S40network restart;                                           }

get_ipaddr()    { ifconfig eth0 | awk '/inet addr/{print substr($2,6)}';                    }

get_ipaddr6()   { ifconfig eth0 | grep inet6 | awk -F '[ \t]+|/' '{print $4}';              }

get_gateway()   { route -n | grep 'UG[ \t]' | awk '{print $2}';                             }

get_dns1()      { cat /etc/resolv.conf | grep ^nameserver | cut -d ' ' -f 2 | sed '1!d';    }

get_dns2()      { cat /etc/resolv.conf | grep ^nameserver | cut -d ' ' -f 2 | sed '2!d';    }

get_netmask()   { ifconfig eth0 | awk '/inet addr/{print substr($4,6)}';                    }

get_lan()
{
        status=`ethtool eth0 | tail -1 | cut -d ' ' -f 3`
        if [ "$status" = "yes" ]; then
                echo "Connected"
        else
                echo "Disconnected"
        fi
}

get_internet()
{
        if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
                echo "Connected"
        else
                echo "Disconnected"
        fi
}

get_dhcp()
{
        tmp=`grep "iface eth0 inet static" /etc/network/interfaces`
        tmp2=`grep "iface eth0 inet6 static" /etc/network/interfaces`
        if [ -z "$tmp" ] && [ -z "$tmp2" ]; then
                echo "TRUE"
        else
                echo "FALSE"
        fi
}

get_hwaddr()        { cat /etc/network/interfaces | awk '{if ($1 ~ /hwaddress/) print $3}'; }

get_info()
{
        case "$1"  in
                ipaddr)
                        get_ipaddr
                        ;;
                ipaddr6)
                        get_ipaddr6
                        ;;
                netmask)
                        get_netmask
                        ;;
                gateway)
                        get_gateway
                        ;;
                internet)
                        get_internet
                        ;;
                lan)
                        get_lan
                        ;;
                dhcp)
                        get_dhcp
                        ;;
                dns1)
                        get_dns1
                        ;;
                dns2)
                        get_dns2
                        ;;
                *)
                        usage
                        ;;
        esac

}

set_static()
{
        cp /etc/network/interfaces /etc/network/interfaces.bak
                
        if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]; then
                usage
                exit 0
        fi

        ifc_file=/root/changeInterfaces.awk
        if [ ! -f $ifc_file ]; then
                exit 0
        fi
        
        ifdown -af
        
        if [ "$1" = "inet4" ]; then
                awk -f $ifc_file /etc/network/interfaces dev=eth0 mode=static inet=4 address="$2" netmask="$3" gateway="$4" > /tmp/interfaces
        else
                awk -f $ifc_file /etc/network/interfaces dev=eth0 mode=static inet=6 address="$2" netmask="$3" gateway="$4" > /tmp/interfaces
        fi
        
        mv /tmp/interfaces /etc/network/interfaces
        cp /etc/network/interfaces /data/configs/ 
        
        ifup -a
}

set_dhcp()
{
        if [ -z "$1" ]; then
                usage
                exit 0
        fi

        ifc_file=/root/changeInterfaces.awk
        if [ ! -f $ifc_file ]; then
                exit 0
        fi
        
        ifdown -af

        rm /etc/resolv.conf
        rm /data/configs/resolv.conf
        touch /etc/resolv.conf

        if [ "$1" = "inet4" ]; then
                awk -f $ifc_file /etc/network/interfaces dev=eth0 mode=dhcp inet=4 > /tmp/interfaces
        else
                awk -f $ifc_file /etc/network/interfaces dev=eth0 mode=dhcp inet=6 > /tmp/interfaces
        fi

        mv /tmp/interfaces /etc/network/interfaces
        cp /etc/network/interfaces /data/configs/

        ifup -a

        udhcpc -i eth0 -t 0 -T 5 -b > /dev/null &
}

set_dns()
{
        ifdown -af

        cp /etc/resolv.conf /etc/resolv.bak

        if [ -z "$1" ] && [ -z "$2" ]; then
                echo "" > /etc/resolv.conf
        elif [ -z "$2" ]; then
                echo "nameserver $1" > /etc/resolv.conf
        else 
                echo "nameserver $1" > /etc/resolv.conf
                echo "nameserver $2" >> /etc/resolv.conf
        fi

        cp /etc/resolv.conf /data/configs/
        
        ifup -a
}

set_hwaddr()
{
        cp /etc/network/interfaces /etc/network/interfaces.bak
                
        if [ -z "$1" ] ; then
                usage
                exit 0
        fi

        ifdown -af

        ifc_file=/root/changeInterfaces.awk
        if [ ! -f $ifc_file ]; then
                exit 0
        fi
        
        awk -f $ifc_file /etc/network/interfaces dev=eth0 hwaddress="$1" > /tmp/interfaces
        
        mv /tmp/interfaces /etc/network/interfaces
        cp /etc/network/interfaces /data/configs/
    
        ifup -a

        udhcpc -i eth0 -t 0 -T 5 -b > /dev/null &
}

main()
{
        case "$1" in
                get)
                        if [ "$2" = "hwaddr" ]; then
                                get_hwaddr
                        else
                                get_info "$2" 
                        fi        
                        ;;
                set)
                        if [ "$2" = "static" ]; then
                                set_static "$3" "$4" "$5" "$6"
                        elif [ "$2" = "dhcp" ]; then
                                set_dhcp "$3"
                        elif [ "$2" = "dns" ]; then
                                set_dns "$3" "$4"
                        elif [ "$2" = "hwaddr" ]; then
                                set_hwaddr "$3"        
                        else
                                usage
                        fi
                        ;;
                *)
                        usage
                        ;;
        esac
}

main $*
exit 0

