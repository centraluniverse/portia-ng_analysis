#!/bin/sh
# Script to set WiFi device to AP mode
# Usage: ./WifiAp.sh [config-file]

LOGFILE="/root/portia.log"
LOCALIP="172.16.0.1"
APCONFIG="/data/configs/wilc_hostapd_wpa.conf"
DHCPCONFIG="/etc/udhcpd.conf"

log()	{ echo "$(date). WifiAp.sh: "$@ >> $LOGFILE; }

check_ap()
{
    ret=`ps -o comm | grep hostapd | wc -l`
    if [ "$ret" != 0 ]; then
        exit 0
    fi
}
main()
{
	if [ ! -z "$1" ] &&  [ -f "$1" ]; then
		APCONFIG=$1
	fi

	ifconfig wlan0 2>&1 >/dev/null
	if [ $? -eq 0 ]; then
		log "Starting WiFi AP.."
		ifconfig wlan0 $LOCALIP
		/usr/sbin/hostapd -B $APCONFIG
		udhcpd $DHCPCONFIG
	else
		log "Error with wlan0 device.."
	fi
}

check_ap

main $1

exit 0
