#!/bin/sh

# This script handles wilc1000 wireless configuration

config="/etc/wilc_wpa_supplicant.conf"
driver="/lib/modules/4.4.26-linux4sam_5.5/kernel/drivers/net/wireless/atmel/wilc1000/wilc1000.ko"

usage()
{
	echo "-----------------------------------------------------"
	echo "Usage:"
	echo -e "\tconnect <ssid> <password> - Connect to a wireless network with password"
	echo -e "\t\t (Use 'static' or 'dhcp' commands to configure the connection)"
	echo -e "\tdisconnect - Disconnect from the wireless network"
	echo -e "\tstatic <ip> <netmask> <gateway> - Use static configuration for the device"
	echo -e "\tdhcp - Use DHCP for configuring the wireless device"
	echo -e "\tstart - Load wilc1000.ko module"
	echo -e "\tstop - Unload wilc1000.ko module"
	echo -e "\tscan - Scan for wifi networks (Call scan_results to view results)"
	echo -e "\tscan_results - View last results of wifi scan"
	echo -e "\tstatus - Show device and connection info"
	echo -e "-----------------------------------------------------"
}

wifi_load()			{ insmod $driver; sleep 5; wifi_init; }

wifi_unload()		{ rmmod wilc1000; }

wifi_scan()			{ wpa_cli scan; }

wifi_scan_res()		{ wpa_cli scan_results; }

wifi_dhcp()			{ udhcpc -i wlan0; }

wifi_disconnect()	{ wpa_cli –p/var/run/wpa_supplicant disconnect; }

wifi_static()
{
	if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
		usage
		exit 0
	fi

	ifconfig wlan0 $1 netmask $2
	route add default gw $3 wlan0
}

wifi_init()
{
	wpa_cli -p/var/run/wpa_supplicant status

	if [ $? != 0 ]; then	
		cat > $config <<- EOM
			ctrl_interface=/var/run/wpa_supplicant
			update_config=1
		EOM

		wpa_supplicant -B -Dwext -iwlan0 -c$config
		if [ $? != 0 ]; then
			echo "Failed to start wpa_supplicant (driver not loaded?)"
		fi
	fi
}

# Connect allows all valid ciphers
# If you want to allow only a specific protocol or ssid check example file at
# https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf
wifi_connect()
{
	if [ -z "$1" ] || [ -z "$2" ]; then
		usage
		exit 0
	fi
	
	id=`wpa_cli -p/var/run/wpa_supplicant add_network | tail -1`
	wpa_cli -p/var/run/wpa_supplicant set_network "$id" ssid \"$1\"
	wpa_cli -p/var/run/wpa_supplicant set_network "$id" psk \"$2\"
	wpa_cli -p/var/run/wpa_supplicant select_network "$id"
}

wifi_status()
{
	ifconfig wlan0
	echo ""
	wpa_cli status
}

main()
{
	case "$1" in
		connect)
			wifi_connect "$2" "$3"
			;;
		disconnect)
			wifi_disconnect
			;;
		dhcp)
			wifi_dhcp
			;;
		static)
			wifi_static "$2" "$3" "$4"
			;;
		start)
			wifi_load
			;;
		stop)
			wifi_unload
			;;
		scan)
			wifi_scan
			;;
		scan_results)
			wifi_scan_res
			;;
		status)
			wifi_status
			;;
		*)
			usage
			;;
	esac
}

wifi_init
main $*
exit 0

