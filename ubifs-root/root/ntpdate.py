#!/usr/bin/env python

import datetime
import ntplib
import sys
from management import env

try:
    if len(sys.argv) == 2:
        server = sys.argv[1]

    else:
        # print("Using file: " + env.NTP_PATH)
        with open(env.NTP_PATH, 'r') as f:
            server = f.readline(64).splitlines()[0]

    # print ("Using NTP server: " + server)

    c = ntplib.NTPClient()
    response = c.request(server, version=3, timeout=5)
    t = datetime.fromtimestamp(response.orig_time)

    setdate = 'date -s ' + '"' + t.strftime("%Y-%m-%d %H:%M:%S") + '"'
    # print("Executing: " + setdate)
    subprocess.Popen([setdate], shell=True)

    sethwclock = "hwclock -w"
    # print("Executing: " + sethwclock)
    subprocess.Popen([sethwclock], shell=True)

except Exception as e:
    pass
    # print ("Failed to set NTP time: " + str(e))

