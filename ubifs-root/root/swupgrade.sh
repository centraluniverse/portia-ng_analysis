#!/bin/sh

# System upgrade script for Sama5d2 Portia + SWUpdate
# Usage: ./swupgrade.sh <package.swu>

LOGFILE="/root/portia.log"

log()	{ echo "swupgrade.sh: " $@ >> $LOGFILE; }

# Write to log
log `date`
log "Upgrading with software package: $1"

# Check upgrade package exists
if [ -z "$1" ] ||  [ ! -f "$1" ]; then
	log "Error: Invalid file ($1)"
	exit 1
fi

# Get target partition
if grep -q "root=ubi0:rootfs1" /proc/cmdline; then
	SWTARGET="copy2"
elif grep -q "root=ubi0:rootfs2" /proc/cmdline; then
	SWTARGET="copy1"
else
	log "Error: Could not find active partition"
	exit 1
fi

# Get target device tree
DTB_TARGET=`grep "dtb" /etc/sw-versions | sed 's/.* //g'`

# Run upgrades
log "Upgrading DTB target: $DTB_TARGET"
swupdate -v --select $DTB_TARGET,$SWTARGET -i $1

log "Upgrading RootFS target: $SWTARGET"
swupdate -v --select sama5d2,$SWTARGET -i $1

# Check if update completed successfully
if [ $? -eq 0 ]; then
	log "Upgrade Success.. rebooting!"
	reboot
fi

log "Upgrade Failed.. rebooting!"

sleep 5
reboot

