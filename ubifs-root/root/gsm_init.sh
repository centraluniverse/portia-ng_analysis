#!/bin/sh

#export gpios for GSM module:

echo 1 > /sys/class/leds/gsm_pwr/brightness

echo 1 > /sys/class/leds/gsm_ctrl/brightness

echo 1 > /sys/class/leds/gsm_vccen/brightness

echo 0 > /sys/class/leds/gsm_reset/brightness

