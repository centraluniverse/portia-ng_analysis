#!/bin/sh

# This script test network device for correct operation and connection

usage()
{
        echo "-----------------------------------------------------"
        echo "Usage:"
        echo "$0 <device> - Test network device"
        echo "-----------------------------------------------------"
		exit 1
}

result()
{
        i=`expr 80 - ${#1}`
        if [ $2 != 0 ]; then
                printf "$1%*s\n" $i '[Fail]'
        else
                printf "$1%*s\n" $i '[Pass]'
        fi
}

test_dev()
{
        ifconfig $DEV > /dev/null 2>&1
        result "Device Test (ifconfig)" $?
}

test_dns()
{
        nslookup www.google.com > /dev/null 2>&1
        result "DNS Test" $?
}

test_ping()
{
        ping -I $DEV -c 1 -w 2 8.8.8.8 > /dev/null 2>&1
        result "Ping Test" $?
}

main()
{
        if [ -z "$DEV" ]; then
                usage
        fi

        test_dev
        test_dns
        test_ping
}

DEV=$1
main

exit 0

